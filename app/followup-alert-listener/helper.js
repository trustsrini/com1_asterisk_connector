"use strict";
var axios = require("axios");
var FormData = require("form-data");
var config = require("../config");

function invokeAlert(row){
    // console.log(row);
    const {extension,type,name,remarks,software,mobile,email} = row;
    const formData = new FormData();
    formData.append("type", type);
    formData.append("receiver", extension);
    formData.append("name", name);
    formData.append("mobile", mobile);
    formData.append("email", email);
    formData.append("remarks", remarks);
    formData.append("software", software);
    formData.append("followupAlert", "true");
    axios.post(config.invoke_endpoint, formData, {
        headers: formData.getHeaders(),
    });
    // console.log(res);
}
function manageEvent(event) {
    var affectedRow = event.affectedRows[0];
    var after = affectedRow.after;
    var before = affectedRow.before;
    if (after) {
        invokeAlert(after);
    } else {
        console.log(before, "Deleted");
    }
    return true;
}
module.exports = { manageEvent};
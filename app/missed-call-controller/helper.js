"use strict";
var axios = require("axios");
var FormData = require("form-data");
var config = require("../config");
var country_code = config.country_code; 

function invokeAlert(row){
    const { src } = row;
    const formData = new FormData();
    var mobile = src.substr(src.length - country_code);
    formData.append("mobile", mobile);
    axios.post(config.missedcall_alert_endpoint, formData, {
        headers: formData.getHeaders(),
    });
}
function manageEvent(event) {
    var affectedRow = event.affectedRows[0];
    var after = affectedRow.after;
    var before = affectedRow.before;
    if (after) {
        invokeAlert(after);
    } else {
        console.log(before, "Deleted");
    }
    return true;
}
module.exports = { manageEvent};
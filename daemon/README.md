<!-- to enable the service -->

systemctl enable asterisk-connector.service

<!-- to start the service  -->

systemctl start asterisk-connector.service

<!-- to check the status of the service -->

systemctl status asterisk-connector.service

<!-- to stop the service -->

systemctl stop asterisk-connector.service

<!-- request example -->

<!-- curl --location --request POST 'http://10.8.0.12/caller/call_controller/call_controller_hook.php' --form 'mobile=9626884884' --form 'receiver=202' -->

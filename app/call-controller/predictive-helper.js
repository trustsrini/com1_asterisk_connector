"use strict";
var axios = require("axios");
var FormData = require("form-data");
var config = require("../config");
var ami = require('../helpers/call-listener');
var country_code = config.country_code; 

function isValidEvent(event) {
  var isValid = true, isNotEmpty = Object.keys(event).length ? true : false;
  var requiedChilds = ["channel", "destchannel", "calleridnum", "linkedid"];
  requiedChilds.forEach(e => {
    if (!event.hasOwnProperty(e)) isValid = false;
  });
  return isValid && isNotEmpty;
}

function getAgentIDFromChannel(channel) {
  return channel.split("/")[1].split("-")[0];
}

function incomingCallData(event, action) {
  if (isValidEvent(event)) {
    const formData = new FormData();
    const { channel, destchannel, calleridnum, linkedid, context,membername} = event;
    var extension = getAgentIDFromChannel(destchannel);
    var mobile = calleridnum.substr(calleridnum.length - country_code);
    if(mobile.includes("predective")) return false;
    formData.append("call", "true");
    formData.append("mobile", mobile);
    formData.append("extension", extension);
    formData.append("agent_code", membername);
    formData.append("channel", channel);
    formData.append("destchannel", destchannel);
    formData.append("action", action);
    formData.append("call_id", linkedid);
    formData.append("campaign", event.queue);
	  formData.append("accountcode", event.accountcode);
	if(action == "hangup") {
      formData.append("holdtime", event.holdtime ? event.holdtime : "");
      formData.append("talktime", event.talktime ? event.talktime : "");
      formData.append("call_termination", event.reason ? event.reason : "");
    }
    return formData;
  } else {
    return false;
  }
}
 

function manageCallEvent(event, action) {
  var formData = new FormData();
  formData = incomingCallData(event, action);
  if (formData) {
    axios.post(config.invoke_endpoint, formData,{headers: formData.getHeaders()})
    .then(function (response) {
          console.log(`Predecitive - ${response.status} - ${response.statusText}`);
    })
  }
}

module.exports = { manageCallEvent };

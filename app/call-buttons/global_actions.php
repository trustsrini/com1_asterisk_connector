<?php

    session_start();

    $request_body = file_get_contents('php://input');

    $data = json_decode($request_body, true); 

    $agent_extension = $data['agent_extension'];
 
    $connection = mysqli_connect("localhost", "root", "#*1234Abcd*#", "com1system") or die(mysqli_error($connection));

    if($agent_extension != '') {

        $phone_type_query = "SELECT extensions.`type` FROM extensions WHERE extension = $agent_extension";

        $phone_type_query_exe = mysqli_query($connection, $phone_type_query);

        $get_phone_type = mysqli_fetch_object($phone_type_query_exe);
         
        if($get_phone_type -> type != 'SIP') {

            if($data['action'] == 'transfer_confirmation') {
                                
                $get_phone_type -> action = "atxtransfer_confirmation"; 
                                
            }
            
            $call_status_query = "SELECT extensions.`hint_status` FROM extensions WHERE extension = $agent_extension";
        
            $call_status_query_exe = mysqli_query($connection, $call_status_query);
        
            $get_call_status = mysqli_fetch_object($call_status_query_exe);

            if($get_call_status -> hint_status == 'INUSE' || $get_call_status -> hint_status == 'RINGING' || $data['action'] == 'call') {
         
                echo json_encode(array("status" => true,"message" => "Success" ,"data"=> $get_phone_type));
        
            } else {
        
                echo json_encode(array("status" => false,"message" => "Error" ,"data"=>""));
        
            }
            
        } else {

            if($data['action'] == 'transfer_confirmation') {
                                
                $get_phone_type -> action = $data['confirmation'] == "yes" ? "hangup" : "CancelAtxfer";
                
            }

            echo json_encode(array("status" => true,"message" => "Success" ,"data"=> $get_phone_type));
            
        }

    } else {

        echo json_encode(array("status" => false,"message" => "falied" ,"data"=>""));

    }
        
?>
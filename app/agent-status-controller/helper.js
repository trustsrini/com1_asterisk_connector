"use strict";
var axios = require("axios");
var FormData = require("form-data");
var config = require("../config");

function emitStatus(agentId, status) {
  var formData = new FormData();
  formData.append("agentId", agentId);
  formData.append("status", status);
  formData.append("agentStatusUpdate", "true");
  // console.log(formData);
  axios.post(config.invoke_endpoint, formData, {
    headers: formData.getHeaders(),
  });
  // console.log(result);
}
function manageEvent(event) {
  var affectedRow = event.affectedRows[0];
  var after = affectedRow.after;
  var before = affectedRow.before;
  if (after) {
    emitStatus(after.agentId, after.agentStatus);
  } else {
    emitStatus(before.agentId, "BREAK");
  }
  return true;
}
module.exports = { manageEvent };

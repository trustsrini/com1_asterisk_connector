#!/usr/bin/bash

# wget http://phone.aboss.in/repos/install.sh
deploy(){

    # colors
    red=`tput setaf 1`

    green=`tput setaf 2`
    
    reset=`tput sgr0`

    PASS="Aboss@3006"

    echo -n Password: 

    read -s password

    echo ""

    if [ "$password" == "$PASS" ] ; then

    echo "${green}Started ${reset}"

    echo "${green}Installing Asterisk connecter.. ${reset}"    

    cd /var/www/html/

    wget http://phone.aboss.in/repos/asterisk-connector.tar.gz

    tar xvzf asterisk-connector.tar.gz

    rm asterisk-connector.tar.gz

    mv asterisk-connector/client/ clients

    rm -rf configs

    mkdir configs

    mv clients/$1/* configs

    rm -rf clients/

    if [ $1 == "template" ] ; then

        cd configs

        sed 's/${IP}/'$3'/g' config.json > config_new.json && mv config_new.json config.json

        sed 's/${IP}/'$3'/g' dev.json > dev_new.json && mv dev_new.json dev.json        

        cd configs                

        sed 's/${CLIENT_NAME}/'$2'/g' disposition.json > disposition_new.json && mv disposition_new.json disposition.json
            
        cd ..
    fi

    cd /var/www/html

    mv configs/dev.json asterisk-connector/app/config/dev.json

    cd asterisk-connector

    npm i

    cd daemon

    sudo chmod +x install.sh

    sudo ./install.sh

    echo "${green}Asterisk connecter installation finished.. ${reset}"

    cd /var/www/html
    
    echo "${green}Installing Caller.. ${reset}"

    wget http://phone.aboss.in/repos/caller.tar.gz

    tar xvzf caller.tar.gz

    rm caller.tar.gz

    rm -rf caller/configs
    
    mv configs/configs caller/

    mv configs/config.json caller/event_controller/config.json

    cd caller/event_controller

    npm i

    cd daemon

    sudo chmod +x install.sh

    sudo ./install.sh

    echo "${green}Caller installation finished..${reset}"

    rm -rf /var/www/html/configs/

    echo "${green}Finished ${reset}"

    chown -R apache:apache /var/www/html/caller
    
    rm /root/install.sh

    else

    echo "${red}Incorrect Password ${reset}"

    fi

}

read -p 'Enter client name :' client_name;

read -p 'Enter client ip :' client_ip;

deploy "template" $client_name $client_ip

# systemctl status asterisk-connector.service
# systemctl status caller-eventcontroller.service
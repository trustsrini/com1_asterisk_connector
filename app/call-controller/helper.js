"use strict";
var axios = require("axios");
var FormData = require("form-data");
const fs = require('fs'); 
var config = require("../config");
var ami = require('../helpers/call-listener');
var trunks = config.trunk;
var extensions = config.extension; 
var country_code = config.country_code; 

let manageAction = (req) => new Promise(function (resolve, reject) {
  const { channel } = req.body;
  const { socketIO } = global;
  axios.post(config.host+'/asterisk-connector/app/call-buttons/global_actions.php', req.body).then(function (response) {
    var response_data = response.data;
    console.log(response_data);
    if(response_data.status) {
      var phone_type = response_data.data.type;
      var call_action = response_data.data.action; 
      if(call_action !='' && call_action != undefined) req.body.action = call_action;         
      const { action } = req.body;
      if(phone_type != 'SIP') {
        socketIO.emit("call_actions", req.body); 
        resolve({ message: response_data.message, data: req.body });
      } else {
        switch (action) {
          case "hangup":
            ami.ami.action({
              'action': 'hangup',
              'channel': channel,
            }, function (err, res) {
              if (err) reject(err);
              resolve({ message: "Success", data: res });
            });
            break;
          case "mute":
            ami.ami.action({
              'action': 'MuteAudio',
              'actionID': 'test123',
              'channel': channel,
              'direction': 'all',
              'state': 'on'
            }, function (err, res) {
              if (err) reject(err);
              resolve({ message: "Success", data: res });
            });
            break;
          case "unmute":
            ami.ami.action({
              'action': 'MuteAudio',
              'actionID': 'test123',
              'channel': channel,
              'direction': 'all',
              'state': 'off'
            }, function (err, res) {
              if (err) reject(err);
              resolve({ message: "Success", data: res });
            });
            break;
          case "call":
            const { mobile, agent_extension } = req.body;
            ami.ami.action({
              'action': 'originate',
              'channel': 'PJSIP/' + agent_extension,
              'context': 'agentfunnelcallback',
              'Exten': mobile,
              'Priority': '1',
              'Callerid': mobile
            }, function (err, res) {
              if (err) reject(err);
              resolve({ message: "Success", data: res });
            });
            break;
          case "atxtransfer":
            const { extension } = req.body;
            ami.ami.action({
              'action': 'Atxfer',
              'channel': channel,
              'context': 'internal',
              'exten': extension,
              'priority': '1'
            }, function (err, res) {
              if (err) reject(err);
              resolve({ message: "Success", data: res });
            });
            break;
          case "blindtransfer":
            var exten = req.body.extension;
            ami.ami.action({
              'action': 'Blindtransfer',
              'channel': channel,
              'context': 'internal',
              'exten': exten,
              'priority': '1'
            }, function (err, res) {
              if (err) reject(err);
              resolve({ message: "Success", data: res });
            });
            break;
          case "exttransfer":
            var mobile_no = req.body.mobile;
            var agent_extension_no = req.body.agent_extension;
            console.log(req.body);
            ami.ami.action({
              'action': 'originate',
              'channel': 'PJSIP/' + agent_extension_no,
              'context': 'extcalltransfer',
              'Exten': mobile_no,
              'Priority': '1',
              'Callerid': mobile_no
            }, function (err, res) {
              console.log(res);
              if (err) reject(err);
              resolve({ message: "Success", data: res });
            });
            break;
          case "hold":
            const { destchannel } = req.body;
            ami.ami.action({
              'action': 'Park',
              'channel': destchannel,
              'timeoutchannel': channel,
              'timeout': 90000,
              'actionID': 'test123'
            }, function (err, res) {
              if (err) reject(err);
              resolve({ message: "Success", data: res });
            });
            break;
          case "unhold":
            ami.ami.action({
              'Action': 'Bridge',
              'ActionID': 'test123',
              'Channel1': req.body.destchannel,
              'Channel2': req.body.channel,
              'Tone': 'Both'
            }, function (err, res) {
              if (err) reject(err);
              resolve({ message: "Success", data: res });
            });
          break;
          case "bridge":
            ami.ami.action({
              'Action': 'Bridge',
              'ActionID': 'test123',
              'Channel1': req.body.channel1,
              'Channel2': req.body.channel2,
              'Tone': 'Both'
            }, function (err, res) {
              if (err) reject(err);
              resolve({ message: "Success", data: res });
            });
          break;
          case "CancelAtxfer":
            ami.ami.action({
              'Action': 'CancelAtxfer',
              'ActionID': 'test123',
              'Channel': req.body.channel,
            }, function (err, res) {
              if (err) reject(err);
              resolve({ message: "Success", data: res });
            });
            break;
          default:
            reject("Invalid action");
        }
      }
    } else {
      resolve({ message: response_data.message, data: [] });
    }
  }).catch(err => console.log(err));

});

function getExtensionFromChannel(channel) {
  if (channel !== undefined && channel !='') return channel.split("/")[1].split("-")[0];
  else "000";
}

function isTrunk(value) {
  return trunks.indexOf(value) != -1;
}

function isExtension(value) {
  return extensions.indexOf(value) != -1;
}

function getCallType(channel, destChannel) {
  var extension = getExtensionFromChannel(channel);
  var dest_extension = getExtensionFromChannel(destChannel);
  var type = false;
  if (isExtension(extension) && isTrunk(dest_extension)) type = "dialed";
  else if (isExtension(dest_extension) && isTrunk(extension)) type = "received";
  else if (isExtension(extension) && isExtension(dest_extension)) type = "inter-communication";
  else if (isExtension(extension) && !isTrunk(extension)) type = "dialed";
  else if (isExtension(extension)) type = "dialed";
  else if (isTrunk(extension)) type = "received";
  return type;
}

function getCallState(action) {
  if (action == "calling") return "ringing";
  else if (action == "ongoing") return "answered";
  else if (action == "hangup") return "ended";
  else return action;
}

function generateFormData(mobile, extension, callType, action, event, state = "") {
  const context = event.context ? event.context : event.destinationcontext;
  if (callType !== false && callType !== undefined && mobile.length >= 3 && context.search("from") == -1 && context.search("AD_") == -1 && context.search("PD_") == -1) {
    const formData = new FormData();
    state = (state == "" ? getCallState(action) : state);
    if (mobile.includes("predective")) return false;
    formData.append("call", "true");
    formData.append("mobile", mobile);
    formData.append("extension", extension);
    formData.append("action", action);
    formData.append("call_id", event.linkedid ? event.linkedid : event.uniqueid);
    formData.append("channel", event.channel ? event.channel : "");
    formData.append("destchannel", event.destinationchannel ? event.destinationchannel : event.destchannel);
    formData.append("context", context);
    formData.append("type", callType);
    formData.append("state", state);
    if (action == "hangup") {
      formData.append("accountcode", event.accountcode ? event.accountcode : "");
      formData.append("starttime", event.starttime ? event.starttime : "");
      formData.append("answertime", event.answertime ? event.answertime : "");
      formData.append("endtime", event.endtime ? event.endtime : "");
      formData.append("duration", event.duration ? event.duration : "");
      formData.append("billableseconds", event.billableseconds ? event.billableseconds : "");
      formData.append("disposition", event.disposition ? event.disposition : "");
      formData.append("userfield", event.userfield ? event.userfield : "");
    } else if (state == "noanswer" || state == "rejected" || state == "busy") {
      formData.append("starttime", event.starttime ? event.starttime : "");
    }
    return formData;
  } else return false;
}

function dialBeginEventData(event) {
  if (event.channelstate == "4" || event.channelstate == "6") {
    var callType = getCallType(event.channel, event.destchannel);
    var mobile = "", extension = "";
    if (callType == "dialed") {
      var extension = getExtensionFromChannel(event.channel);
      var mobile = event.exten.substr(event.exten.length - country_code);
    } else if (callType == "received") {
      var extension = getExtensionFromChannel(event.destchannel);
      var mobile = event.calleridnum.substr(event.calleridnum.length - country_code);
    } else if (callType == "inter-communication") {
      var extension = getExtensionFromChannel(event.destchannel);
      var mobile = event.calleridnum.substr(event.calleridnum.length - country_code);
      callType = "received";
    }
    // console.log(mobile,extension,callType,"calling");
    return generateFormData(mobile, extension, callType, "calling", event);
  } else {
    return false;
  }
}

function dialEndEventData(event) {
  if ((event.channelstate == "4" || event.channelstate == "6") && ["ANSWER"].indexOf(event.dialstatus) != -1) {
    var callType = getCallType(event.channel, event.destchannel);
    var mobile = "", extension = "";
    if (callType == "dialed") {
      var extension = getExtensionFromChannel(event.channel);
      var mobile = event.exten.substr(event.exten.length - country_code);
    } else if (callType == "received") {
      var extension = getExtensionFromChannel(event.destchannel);
      var mobile = event.calleridnum.substr(event.calleridnum.length - country_code);
    } else if (callType == "inter-communication") {
      var extension = getExtensionFromChannel(event.destchannel);
      var mobile = event.calleridnum.substr(event.calleridnum.length - country_code);
      callType = "received";
    }
    var action = "";
    var state = "";
    if (event.dialstatus == "ANSWER") {
      action = "ongoing";
      state = getCallState("ongoing");
    }
    // console.log(mobile,extension,callType,action);
    return generateFormData(mobile, extension, callType, action, event, state);
  } else {
    return false;
  }
}

function hangupEventData(event) {
  var mobile = "";
  if (event.destination.length >= country_code) {
    var callType = "dialed";
    var extension = getExtensionFromChannel(event.channel);
    var mobile = event.destination.substr(event.destination.length - country_code);
  } else {
    var callType = "received";
    var extension = getExtensionFromChannel(event.destinationchannel);
    var mobile = event.source.substr(event.source.length - country_code);
  }

  var action = "";
  var state = "";
  if (event.disposition == "ANSWERED") {
    action = "hangup";
    state = "ended";
  } else if (event.disposition == "BUSY" && callType == "dialed") {
    action = "hangup";
    state = "rejected";
  } else if (event.disposition == "BUSY" && callType == "received") {
    action = "hangup";
    state = "missed";
  } else if (event.disposition == "CANCEL") {
    action = "hangup";
    state = "busy";
  } else if (event.disposition == "NO ANSWER" && event.duration >= 50 && callType == "dialed") {
    action = "hangup";
    state = "noanswer";
  } else if (event.disposition == "NO ANSWER" && event.duration < 50 && callType == "dialed") {
    action = "hangup";
    state = "busy";
  } else if (event.disposition == "NO ANSWER" && callType == "received") {
    action = "hangup";
    state = "missed";
  }
  return generateFormData(mobile, extension, callType, action, event, state);
}

function attendedTransferEventData(event) {
  if (event.origtransfererchannelstate == "6") {
    // if(event.destchannel == undefined) event.destchannel = "SIP/"+event.connectedlinenum+"-00000000";
    var mobile = "";
    var callType = "received";
    var extension = getExtensionFromChannel(event.origtransfererchannel);
    var mobile = event.transfertargetcalleridnum.substr(event.transfertargetcalleridnum.length - country_code);
    event.linkedid = event.transfertargetuniqueid;
    event.context = event.transfertargetcontext;
    event.channel = event.origtransfererchannel;
    event.destchannel = event.transfereechannel;

    console.log(mobile, extension, callType, "hangup");
    return generateFormData(mobile, extension, callType, "hangup", event);
  } else {
    return false;
  }
}

function manageCallEvent(event, action) {
  var formData = new FormData();
  if (action == "dialBegin")
    formData = dialBeginEventData(event);
  else if (action == "dialEnd")
    formData = dialEndEventData(event);
  else if (action == "hangup")
    formData = event.destinationchannel != '' ? hangupEventData(event) : false;
  else if (action == "attendedTransfer")
    formData = attendedTransferEventData(event);
  else formData = false;
  if (formData) {
    axios.post(config.invoke_endpoint, formData, { headers: formData.getHeaders() }).then(function (response) {
      console.log(`${response.status} - ${response.statusText}`);
      console.log(`response`, response.body);
    }).catch(function (error) {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        // console.log(error.response.data);
        // console.log(error.response.status);
        // console.log(error.response.headers);
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        // console.log(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        // console.log('Error', error.message);
      }
      // console.log(error.config);
    });
  }
}

// NODE_TLS_REJECT_UNAUTHORIZED='0' node /var/www/html/asterisk-connector/

function hrncMissedCallData(event) {
  const { calleridnum } = event;
  const formData = new FormData();
  var mobile = calleridnum.substr(calleridnum.length - country_code);
  formData.append("mobile", mobile);
  formData.append("hrnc-missedcall", "true");
  return formData;
}

function manageHRNCEvent(event, action) {
  var formData = new FormData();
  if (action == "hrnc-missedcall") {
    formData = hrncMissedCallData(event);
    if (formData) {
      axios.post(config.hrce_endpoint, formData, { headers: formData.getHeaders() })
        .then(function (response) {
          console.log(`${response.status} - ${response.statusText}`);
        })
    }
  }
}

module.exports = { manageAction, manageCallEvent, manageHRNCEvent };
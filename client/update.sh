#!/usr/bin/bash

update(){

    # colors
    red=`tput setaf 1`

    green=`tput setaf 2`
    
    reset=`tput sgr0`

    PASS="Aboss@3006"

    echo -n Password: 

    read -s password

    echo ""

    if [ "$password" == "$PASS" ] ; then

    echo "${green}Started ${reset}"

    systemctl stop asterisk-connector.service

    systemctl stop caller-eventcontroller.service

    cd /var/www/html/

    rm -rf caller_backup

    mkdir caller_backup

    mkdir caller_backup/configs

    cp asterisk-connector/app/config/dev.json caller_backup/configs

    cp caller/configs caller_backup/configs

    cp caller/event_controller/config.json caller_backup/configs

    mkdir caller_backup/code

    echo "${green}Updating Asterisk connecter.. ${reset}"    

    mv asterisk-connector caller_backup/code/

    wget http://phone.aboss.in/repos/asterisk-connector.tar.gz

    tar xvzf asterisk-connector.tar.gz

    rm asterisk-connector.tar.gz

    rm asterisk-connector/client/

    rm -rf configs

    mkdir configs

    cp caller_backup/configs configs

    mv configs/dev.json asterisk-connector/app/config/dev.json

    cd asterisk-connector

    npm i

    echo "${green}Asterisk connecter Update finished.. ${reset}"
    
    echo "${green}Updating Caller.. ${reset}"

    mv caller caller_backup/code/

    wget http://phone.aboss.in/repos/caller.tar.gz

    tar xvzf caller.tar.gz

    rm caller.tar.gz

    rm -rf caller/configs
    
    mv configs/configs caller/

    mv configs/config.json caller/event_controller/config.json

    cd caller/event_controller

    npm i

    echo "${green}Caller Update finished..${reset}"

    rm -rf /var/www/html/configs/

    echo "${green}Finished ${reset}"

    chown -R apache:apache /var/www/html/caller
    
    rm /root/update.sh

    systemctl start asterisk-connector.service

    systemctl start caller-eventcontroller.service

    else

    echo "${red}Incorrect Password ${reset}"

    fi

}

update

# systemctl status asterisk-connector.service
# systemctl status caller-eventcontroller.service
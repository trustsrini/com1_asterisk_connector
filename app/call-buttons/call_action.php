<?php

  session_start();

  include_once __DIR__."/../../../event-controller/config.php";

  extract($_POST);
  
  $ip = $listener_config['actions_ip'];

  $url = $ip.'/callContoller/action';

  $type = $_SESSION['phonetype'];

  $dial_prefix = $_SESSION['DialPrefix'];

  $login_details = json_decode($_SESSION['login_details']);

  $dial_digit = $login_details -> DialDigits;


   $mobile = (strlen($mobile) > ($dial_digit - 1)) ? $dial_prefix.''.$mobile : $mobile;

  if($type == 'WEBRTC') {
    
    $btn_event = json_decode($datas);  
    
    if($btn_event !='') {
     
      if(strlen($btn_event -> extension) > ($dial_digit - 1)) {    

        $btn_event -> extension = $dial_prefix.''.$btn_event -> extension;

      }  

      $post_fields = 'action='.$btn_event -> action.'&extension='.$btn_event -> extension.'&confirmation='.$btn_event -> confirmation.'&agent_extension='.$btn_event -> agent_extension;


    } else {      
  
      $post_fields = 'action='.$action.'&extension='.$mobile.'&agent_extension='.$agent_extension;

    }
    
  } else {
    
    $post_fields = 'action='.$action.'&channel='.$channel.'&destchannel='.$destchannel.'&channel1='.$channel1.'&channel2='.$channel2.'&extension='.$extension.'&mobile='.$mobile.'&agent_extension='.$agent_extension;
      
  }

  $curl = curl_init();
  
  curl_setopt_array($curl, array(
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10, 
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => $post_fields,
    CURLOPT_HTTPHEADER => array(
      'Content-Type: application/x-www-form-urlencoded'
    ),
  ));

  $response = curl_exec($curl);

  curl_close($curl);

  echo $response;

?>
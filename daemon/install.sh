#!/usr/bin/sh
cp asterisk-connector.service /etc/systemd/system/asterisk-connector.service

systemctl enable asterisk-connector.service

systemctl start asterisk-connector.service

# systemctl status asterisk-connector.service

function call_mute() {
    $(".call_buttons").prop("disabled", true);
    $('.call_buttons').attr("disabled","disabled");
    $("#call_mute").hide();
    $("#call_unmute").show();
    $("#call_unmute").prop("disabled", false);
    var channel = $("#call_channel").val();
    var extension = $("#extension").val();
    $.ajax({
        url: "/asterisk-connector/app/call-buttons/call_action.php",
        type: "POST",
        data: {
            action: "mute",
            channel: channel, 
            agent_extension:extension
        },
        success: function (data) {
        }
    })
}
function call_unmute() {
    $(".call_buttons").prop("disabled", false);
    $("#call_mute").show();
    $("#call_unmute").hide();
    var channel = $("#call_channel").val();
    var extension = $("#extension").val();
    $.ajax({
        url: "/asterisk-connector/app/call-buttons/call_action.php",
        type: "POST",
        data: {
            action: "unmute",
            channel: channel,
            agent_extension:extension
        },
        success: function (data) {
        }
    })
}


function call_hold(data='',id='') {    
    if(data!='') {
        if(id !='') {
            $(".call_hold_"+id).hide();
            $(".calling_btn").hide();
            $(".call_btn_"+id).css("display", "block");
        } else {
            $("#call_hold_transfer").hide();
            $(".call_dial").css("display", "block");
            $(".calling_btn").hide();
        }
    } else {
        $("#call_unhold").show();
        $("#call_hold").hide();
        $(".call_buttons").prop("disabled", true);
        $("#call_unhold").prop("disabled", false);
    }
    var call_type = $("#call_type").val();
    var call_destchannel = $("#call_destchannel").val();
    var call_channel = $("#call_channel").val();    
    $("#transfer_channel_1").val('');
    if(call_type == 'received') {
       $("#transfer_channel_1").val(call_channel);
        var channel = call_destchannel;
       var destchannel = call_channel;
    } else {
        $("#transfer_channel_1").val(call_destchannel);
        var channel = call_channel;
        var destchannel = call_destchannel;
    }
    var extension = $("#extension").val();  
    $.ajax({
        url: "/asterisk-connector/app/call-buttons/call_action.php", 
        type: "POST",
        data: {
            action: "hold",
            channel: channel,
            destchannel: destchannel,
            agent_extension:extension
        },
        success: function (data) {
            //call_hangup(channel);
        }
    })
}

function call_unhold() {
    $(".call_buttons").prop("disabled", false);
    $("#call_unhold").hide();
    $("#call_hold").show();    
    var destchannel = $("#call_destchannel").val();
    var channel = $("#call_channel").val();
    $.ajax({
        url: "/asterisk-connector/app/call-buttons/call_action.php",
        type: "POST",
        data: {
            action: 'unhold',
            destchannel: destchannel,
            channel: channel,
            agent_extension:$("#extension").val()
        },
        success: function (data) {

        }
    })
}

function call_hangup(cid='', mobile='') {
    // var channel = $("#call_destchannel").val();
    var atx_channel = $("#atx_channel").val();
    var destchannel = $("#call_destchannel").val();
    var channel = $("#call_channel").val();
    if(atx_channel != '') {
        var channel = channel;   
        $("#atx_channel").val("");    
    } else {
        var channel = destchannel;
    }
    var call_status = '2';
    $.ajax({
        //  url:ip + "/callContoller/action",
        url: "/asterisk-connector/app/call-buttons/call_action.php",
        type: "POST",
        data: {
            action: "hangup",
            channel: channel,
            agent_extension:$("#extension").val()
        },
        success: function (data) {
            if(cid !=''&& mobile !='') {
                setTimeout(function () {
                    $.ajax({
                        url: "helpers/campaigncreated_quickdisposition.php",
                        type: "POST",
                        data: {},
                        success: function (data) {
                            $("#call_transfer_modal").hide();
                            if (data == "Y") {
                                showDispositionForm(-1, cid, mobile, call_status);
                                location.reload();
                            }
                        }
                    })
                }, 2000);                
            }
        }
    })
}

function extTransferCall(number = null,id=null) {
    var dial_digit = $("#dial_digit").val();
    if(id != null) {
        $(".call_transfer_icon_"+id).css("display","block");
        $(".call_btn_"+id).css("display","none");
    } else {
        $(".call_transfer_icon").css("display","block");
        // $("#ext_transfer_call").css("display","none");
    }
    if(number == null) {
        var mobile = $("#transfer_call_dial").val();
    } else {
        var mobile = number;
    }    
    if(mobile.length == dial_digit){
        $.ajax({
            url: "/asterisk-connector/app/call-buttons/call_action.php",
            type: "POST",
            data: {
                action: 'exttransfer',
                mobile: mobile,
                agent_extension:$("#extension").val()
            },
            success: function (data) {
    
            }
        })
    } else {
        alert("Check the mobile number");
    }
}

function calling_popup(extension=null) { 
    $('#call_transfer_modal_body').load('/asterisk-connector/app/call-buttons/orginate_call_popup.php',{extension:extension});
    $("#call_transfer_head").text('');
    $("#call_transfer_head").text('Extension Status');
    $('#call_transfer_modal').modal('show');
}

function call_transfer_popup(phone_type = null,extension=null) { 
    $('#call_transfer_modal_body').load('/asterisk-connector/app/call-buttons/call_transfer_popup.php',{phone_type:phone_type,extension:extension});
    $("#call_transfer_head").text('');
    $("#call_transfer_head").text('Call Transfer');
    $('#call_transfer_modal').modal({
        backdrop: 'static',
        keyboard: false
    });
    $('#call_transfer_modal').modal('show');
}

function extension_blindtransfer(extension='') {
    $("#call_dial_input").css('border', '');
    var error = false;
    if (extension == '') {
        var extension = $("#call_dial_input").val();
        if(extension =='') {
            $("#call_dial_input").css('border', '1px solid red');
            error = true;
        }
    }
    var destchannel = $("#call_destchannel").val();
    var channel = $("#call_channel").val();    
    var call_type = $("#call_type").val();   
    if(call_type == 'received') {
        var channel = destchannel;
    } else {
        var channel = channel;
    }
    if(!error) {
        $.ajax({
            url: "/asterisk-connector/app/call-buttons/call_action.php",
            type: "POST",
            data: {
                action: "blindtransfer",
                channel: channel,
                extension: extension,
                agent_extension:$("#extension").val()
            },
            success: function(data) {
                $('#call_transfer_modal').modal('hide');
            }
        })  
    }
      
}

function extension_atxtransfer(extension='',data='') {
    $("#call_dial_input").css('border', '');
    var error = false;
    if (extension == '') {
        var extension = $("#call_dial_input").val();
        if(extension =='') {
            $("#call_dial_input").css('border', '1px solid red');
            error = true;
        }
    }   
    var destchannel = $("#call_destchannel").val();
    var channel = $("#call_channel").val();    
    var call_type = $("#call_type").val();   
    if(call_type == 'received') {
        var channel = destchannel;
    } else {
        var channel = channel;
        $("#atx_channel").val("1");
    }
    if(!error) {
        if(data == "ext_call") {
            $(".ext_call_transfer_"+extension).show();
            $(".calling_btn").hide();
        } else{
            $(".calling_btn").hide();
            $(".call_transfer").show();
            $("#call_dial_input").show();
        }
        $.ajax({
            url: "/asterisk-connector/app/call-buttons/call_action.php",
            type: "POST",
            data: {
                action: "atxtransfer",
                channel: channel,
                extension: extension,
                agent_extension:$("#extension").val()
            },
            success: function(data) {
                // $('#call_transfer_modal').modal('hide');         
            }
        })    
    }
}

function external_transfer() {
    var destchannel = $("#call_destchannel").val();
    var transfer_channel = $("#transfer_channel_1").val();
    var mobile_number = $("#transfer_call_dial").val();
    var channel1 = transfer_channel;
    var channel2 = destchannel;
    $.ajax({
        url: "/asterisk-connector/app/call-buttons/call_action.php",
        type: "POST",
        data: {
            action: "bridge",
            channel1: channel1,
            channel2: channel2, 
            agent_extension:$("#extension").val()
        },
        success: function (data) {
            setTimeout(function () {
                showDispositionForm(-1, '', mobile_number, '2');
                $("#transfer_channel_1").val('');
                $("#mobile_1").val('');
                $("#call_transfer_modal").hide();
                $('#communication_call').val('');
                location.reload();
            }, 5000);
        }
    })
}

function call_cancel_transfer() {
    var channel = $("#call_channel").val();    
    $.ajax({
        url: "/asterisk-connector/app/call-buttons/call_action.php",
        type: "POST",
        data: {
            action: "CancelAtxfer", 
            channel: channel,
            agent_extension:$("#extension").val()
        },
        success: function (data) {
            $('#call_transfer_modal').modal('hide');
        }
    })
}

function webrtc_call(action,data=null,value=null) {
    var event_data = {};
    event_data['action'] = action;
    var error = false;
    $("#transfer_call_dial").css('border', '');
    $("#call_dial_input").css('border', '');
    if(value == null && action != 'conference_confirmation' && action != 'atxtransfer_confirmation') {
        var extension = $('#call_dial_input').val();
        if(extension =='') {
            $("#call_dial_input").css('border', '1px solid red');
            error = true;
        }
    }
    if(data!=null && value != null) {
        $("#call_dial_input").hide();
    }
    if(!error) {
        $(".calling_btn").hide();
        if(action == 'call') {
            event_data['extension'] = value;
            $('#call_transfer_modal').modal('hide');
        }if(action == 'blindtransfer') {        
            var extension = $('#call_dial_input').val();
            event_data['extension'] = value !=null && value !='' ? value : extension;
            $('#call_transfer_modal').modal('hide');
        } else if(action == 'atxtransfer') {    
            if(data == 'ext_call') {       
              $(".ext_call_transfer_"+value).show();
            } else if(data == 'mobile_call') {
              $(".mobile_call_transfer_"+value).show();
            } else if(data == 'alternate_call') {
              $(".alternate_call_transfer_"+value).show();
            } else {
                $("#conference_call").hide();
                $("#call_dial_input").show();
                $(".call_transfer").show();
            }   
            var extension = $('#call_dial_input').val();
            event_data['extension'] = value !=null && value !='' ? value : extension;
        } else if(action == 'atxtransfer_confirmation') {
            $('#call_transfer_modal').modal('hide');
            event_data['confirmation'] = data;
        } else if(action == 'conference') {       
            if(data == 'ext_call') {       
                $(".ext_call_conference_"+value).show();
            } else if(data == 'mobile_call') {
                $(".mobile_call_conference_"+value).show();
            } else {
                $(".call_conference").show();
            }
            var extension = $('#call_dial_input').val();
            event_data['extension'] = value !=null && value !='' ? value : extension;
        } else if(action == 'conference_confirmation') {
            $('#call_transfer_modal').modal('hide');
            event_data['confirmation'] = data;
        }
        event_data['agent_extension'] = $("#extension").val();  
        $.ajax({
            url: "/asterisk-connector/app/call-buttons/call_action.php",
            type: "POST",
            data: {
                type:'WEBRTC',
                datas:JSON.stringify(event_data)
            },
            success: function (data) {}
        })
    }
    
  }
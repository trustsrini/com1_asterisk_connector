"use strict";

var config = require("./config");
var defaultController = require("./default-controller");
var callController = require("./call-controller/controller");
var agentController = require("./agent-controller/controller");

//setup router for API
module.exports = function (app) {
  //Configure the root route
  app.use("/", defaultController);
  //Adding products route
  app.use("/callContoller", callController);
  app.use("/agentController", agentController);
};

"use strict";

//import required packages
var config = require("./app/config");
const routerConfig = require("./app/router-config");
const cors = require("cors");
var express = require("express");
var bodyParser = require("body-parser"); 
var mysql = require('mysql');
const fs = require('fs');
const io = require("socket.io-client");

var callListener = require("./app/helpers/call-listener");
// var agentStatusListener = require("./app/helpers/agent-status-listener");

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "#*1234Abcd*#", 
  database: "com1system"
});

const socketIO = io(config.host+":3001", { rejectUnauthorized: false});

socketIO.on("connect", () => {
  console.log("Socket connected");
});

socketIO.on("connect_error", (e) => {
  console.log("Error in connecting the socket");
});

socketIO.on('disconnect', () => {
  console.log('Socket disconnected from server');
});

global.socketIO = socketIO;

con.connect(function (err) {
  if (err) throw err;
  con.query("CALL `usp_outgoing_trkgrouping_bindgrid`()", function (err, result, fields) {
    if (err) throw err;
    var trunk = [];
    result[0].forEach(element => {
      trunk.push(element.identity);
    });
    config.trunk = trunk;
    fs.writeFile('/var/www/html/asterisk-connector/app/config/dev.json', JSON.stringify(config), function (err) {
      if (err) throw err;
      console.log('Trunk configured');
    });
  });

  con.query("SELECT extension FROM `extensions` where type != 'Pending'", function (err, result, fields) {
    if (err) throw err;
    var extension = [];
    result.forEach(element => {
      extension.push(element.extension);
    });
    config.extension = extension;
    fs.writeFile('/var/www/html/asterisk-connector/app/config/dev.json', JSON.stringify(config), function (err) {
      if (err) throw err;
      console.log('Extension configured');
    });
  });

  con.query("SELECT extension FROM `extensions` where department = 'Agent-CCS'", function (err, result, fields) {
    if (err) throw err;
    var agent = [];
    result.forEach(element => {
      agent.push(element.extension);
    });
    config.agent = agent;
    fs.writeFile('/var/www/html/asterisk-connector/app/config/dev.json', JSON.stringify(config), function (err) {
      if (err) throw err;
      console.log('Agent configured');
    });
  });
  console.log("Database Connected!");
});

var app = new express();
app.use(cors());

//using body-parser to get POST request data from request
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Load all the routes for app
routerConfig(app);

//start the server
app.listen(config.app_port);
console.log("Started API server on port " + config.app_port);

callListener.listen_calls();
// agentStatusListener.listen_agent_state();
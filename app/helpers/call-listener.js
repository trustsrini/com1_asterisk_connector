var ami = new require("asterisk-manager")(
  "5038",
  "127.0.0.1",
  "admin",
  "golti196",
  true
);
function listen_calls() {
  const fs = require('fs');
  var config = require("../config");
  var callController = require("./../call-controller/helper");
  var predictivecallController = require("./../call-controller/predictive-helper");
  var extensionStatusController = require("./../extension-status-controller/helper");

  // In case of any connectiviy problems we got you coverd.
  ami.keepConnected();
  
  // Listen for any/all AMI events.
 ami.on("managerevent", function (evt) {

  if(config.webhook_playground == "1") {
    fs.appendFile("/var/www/html/event-controller/log.txt","\nReached asterisk: "+ JSON.stringify(evt)+"\n" , (err) => {
      if (err)
        console.log(err);
      else {
        console.log("File written successfully\n");
      }
    });
  }

	switch (evt.event) {
    case "DialBegin":
      callController.manageCallEvent(evt, "dialBegin");
      break;
    case "DialEnd": 
      callController.manageCallEvent(evt, "dialEnd");		
      break;
    case "Cdr":
      callController.manageCallEvent(evt, "hangup");
      break;
    case "AttendedTransfer":
      callController.manageCallEvent(evt, "attendedTransfer");
      break;
    // case "ExtensionStatus": //Extension status change
    //   extensionStatusController.manageEvent(evt);
    //   break; 
    case "DeviceStateChange": //Extension status change
      extensionStatusController.manageEvent(evt);
      break; 
  }
	
	if(config.predective == '1') {
		switch (evt.event) { 
      case "AgentCalled":  //Ringing
        predictivecallController.manageCallEvent(evt, "calling");
          break;
      case "AgentConnect": //Answered
          predictivecallController.manageCallEvent(evt, "ongoing");
          break;
      case "AgentRingNoAnswer": //Agent no answer
          predictivecallController.manageCallEvent(evt, "hangup");
          break;
      case "AgentComplete": //Incoming call hangup
          predictivecallController.manageCallEvent(evt, "hangup");
          break;
      }
    }	

  });
}

module.exports = { listen_calls, ami };

// Listen for specific AMI events. A list of event names can be found at
// https://wiki.asterisk.org/wiki/display/AST/Asterisk+11+AMI+Events
"use strict";
var axios = require("axios");
var FormData = require("form-data");
var config = require("../config");

function emitStatus(extension, status) {
  var formData = new FormData();
  formData.append("extension", extension);
  formData.append("status", status);
  formData.append("extensionStatusUpdate", "true");
  axios.post(config.invoke_endpoint, formData, {
    headers: formData.getHeaders(),
  });
}
function manageEvent(event) {
  if(event.event == 'ExtensionStatus') {
    var extension = event.exten;
    var status = event.statustext;
  } else {
    var device = event.device;
    var extension = (device.search("PJSIP") != '-1') ? ((device !== undefined && device !='') ? device.split("/")[1] : '') : '';
    var status = event.state;
  }
  if(extension !='') emitStatus(extension,status);
}

module.exports = { manageEvent };
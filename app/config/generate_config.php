<?php

    $connection = mysqli_connect("localhost", "root", "#*1234Abcd*#","com1api");

    $get_config_details = mysqli_query($connection,'CALL com1api.usp_api_configuration_details()');

	$config_details = mysqli_fetch_assoc($get_config_details);

    $template_json = json_decode(file_get_contents(__DIR__.'/../../client/template/dev.json'), true);

    $template_json = str_replace('$ip',$config_details['server_ipaddress'],$template_json);
	
	$template_json = str_replace('$vl_ip',$config_details['vl_server_link'],$template_json);

    $template_json = str_replace('$caller',$config_details['ccs_download'],$template_json);
	
	$template_json = str_replace('$api_connector',$config_details['api_connector'],$template_json);
		
	$template_json = str_replace('$generic_webhook',$config_details['api_generic_webhook_link'],$template_json);
	
	$template_json = str_replace('$api_webhook_playground',$config_details['api_webhook_playground'],$template_json);

    $template_json['country_code'] = $config_details['DialDigits'];
	
    file_put_contents(__DIR__.'/dev.json', json_encode($template_json));

    $config_josn = json_decode(file_get_contents(__DIR__.'/../../client/template/config.json'), true);

    $json = str_replace('$ip',$config_details['server_ipaddress'],$config_josn); 
	
    file_put_contents('/var/www/html/event-controller/config.json', json_encode($json));   
    
    system('com1_asterisk_connector_restart');
    
?>
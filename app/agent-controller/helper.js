"use strict";
var axios = require("axios");
var FormData = require("form-data");
var config = require("../config");
var api_path = (config.host + "/admin/integration/").replace('https', 'http');

let manageAction = (req) =>
new Promise(function (resolve, reject) {
    if("action" in req.body){
      const formData = new FormData();
      switch (req.body.action) {
		case "master_campaign_list": 
          axios
            .post(api_path + "ccs_master_campaign_list.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
		case "agent_create":
          formData.append("name", req.body.agent_name);
          formData.append("email", req.body.agent_email);
          formData.append("mobile", req.body.agent_mobile);
          formData.append("status", req.body.agent_status);
          formData.append("category", req.body.agent_category);
          axios
            .post(api_path + "ccs_agent_create.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
		case "agent_edit":
          formData.append("code", req.body.agent_code);
          formData.append("name", req.body.agent_name);
          formData.append("email", req.body.agent_email);
          formData.append("mobile", req.body.agent_mobile);
          formData.append("status", req.body.agent_status);
          formData.append("category", req.body.agent_category);
          axios
            .post(api_path + "ccs_agent_edit.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
		case "agent_master_camapign_link_create":
		  formData.append("agent", req.body.agent_code);
          formData.append("campaign", req.body.campaign_code);
          formData.append("weight", req.body.agent_weight);
          formData.append("category", req.body.agent_category);
          axios
            .post(api_path + "ccs_agent_master_camapign_link_create.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
		 case "agent_master_camapign_link_list":
          axios
            .post(api_path + "ccs_agent_master_camapign_link_list.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
		case "sub_camapign_create":
          formData.append("campaigncreated_name", req.body.campaigncreated_name);
          formData.append("campaign_code", req.body.campaign_code);
          formData.append("campaigncreated_type", req.body.campaigncreated_type);
          axios
            .post(api_path + "ccs_sub_camapign_create.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
        case "agent_login":
          formData.append("campaign", req.body.campaign_code);
          formData.append("agent", req.body.agent_code);
		      formData.append("extension", req.body.extension);
          formData.append("ip_status", req.body.ip_status);
          axios
            .post(api_path + "ccs_agent_login.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
		case "agent_list":
          axios
            .post(api_path + "ccs_agent_list.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
		case "sub_campaign_list":
          axios
            .post(api_path + "ccs_sub_campaign_list.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
		case "agent_logout":
          formData.append("agent", req.body.agent_code);
          axios
            .post(api_path + "ccs_agent_logout.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;

		case "agent_pause":
          formData.append("agent", req.body.agent_code);
          formData.append("reason", req.body.pause_reason);
          axios
            .post(api_path + "ccs_agent_pause.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
        case "agent_unpause":
          formData.append("agent", req.body.agent_code);
          axios
            .post(api_path + "ccs_agent_unpause.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
		  case "abandoned_list_pending":
          axios
            .post(api_path + "ccs_abandoned_pending.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
		  case "abandoned_list_full":
          axios
            .post(api_path + "ccs_abandoned_full.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
		case "campaign_pause":
          formData.append("id", req.body.campaign_id);
          axios
            .post(api_path + "ccs_sub_campaign_pause.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
        case "campaign_delete":
          formData.append("id", req.body.campaign_code);
          axios
            .post(api_path + "ccs_sub_campaign_delete.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break; 
        case "ccs_data_add":
          const customer_details = JSON.stringify(req.body);
          formData.append("campaign", req.body.campaign_code ? req.body.campaign_code : "");
          formData.append("name", req.body.name ? req.body.name : "");
          formData.append("email", req.body.email ? req.body.email : "");
          formData.append("whatsapp", req.body.whatsapp ? req.body.whatsapp : "");
          formData.append("datas", customer_details ? customer_details : "");
          axios
            .post(api_path + "ccs_sub_campaign_dataadd.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
		case "login_extension_list":
          axios
            .post(api_path + "ccs_login_extension_list.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
		case "call_disposition":
          formData.append("callref", req.body.callref);
          formData.append("cli", req.body.cli);
          formData.append("remarks", req.body.remarks);
          formData.append("agent", req.body.agent_code);
          formData.append("disposition", req.body.disposition);
          formData.append("rescedule", req.body.rescedule);
          formData.append("blacklist", req.body.blacklist);
          formData.append("forcepause", req.body.forcepause);
		  formData.append("campaigncreatedcode", req.body.campaigncreatedcode);
		  formData.append("account_code", req.body.accountcode);
		  formData.append("digit", req.body.mobile_digits);
          axios
            .post(api_path + "ccs_call_disposition.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
		case "call_disposition_clear":
          formData.append("agent", req.body.agent_code);
          axios
            .post(api_path + "ccs_call_disposition_clear.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
		case "agent_live_list":
		  formData.append("login_data", req.body.login_data);
          axios
            .post(api_path + "ccs_agent_livelist.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
        case "ccs_data_raw":
          formData.append("campaign", req.body.campaign_code);
          axios
            .post(api_path + "ccscampaigndataraw.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
        case "campaign_data_list":
            axios
              .post(api_path + "com1_agent_app/getcampaignlist.php", formData,{
                headers: formData.getHeaders(),
              })
              .then((response) => resolve(response.data))
              .catch((err) => reject(err));
          break;  
        case "campaign_agents":
          formData.append("compaign_code", req.body.compaign_code);
            axios
              .post(api_path + "com1_agent_app/getagentsbycampaign.php", formData,{
                headers: formData.getHeaders(),
              })
              .then((response) => resolve(response.data))
              .catch((err) => reject(err));
          break;
        case "agent_login_with_password":
          formData.append("compaign_code", req.body.compaign_code);
          formData.append("agent_code", req.body.agent_code);
          formData.append("password", req.body.password);
            axios
              .post(api_path + "com1_agent_app/agent_login.php", formData,{
                headers: formData.getHeaders(),
              })
              .then((response) => resolve(response.data))
              .catch((err) => reject(err));
          break; 
        case "agent_status_list":
            axios
              .post(api_path + "com1_agent_app/get_agent_status_list.php", formData,{
                headers: formData.getHeaders(),
              })
              .then((response) => resolve(response.data))
              .catch((err) => reject(err));
          break;
        case "current_agent_status":
          formData.append("agent_code", req.body.agent_code);
          axios
            .post(api_path + "com1_agent_app/current_agent_status.php", formData,{
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
        break;
        case "agent_start":
          formData.append("agent_code", req.body.agent_code);
          axios
            .post(api_path + "com1_agent_app/agent_start.php", formData,{
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
        break;
        case "agent_break":
          formData.append("agent_code", req.body.agent_code);
          formData.append("pause_reason", req.body.pause_reason);
          axios
            .post(api_path + "com1_agent_app/agent_pause.php", formData,{
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
        break;
        case "call_log_list":
          formData.append("mobile", req.body.mobile);
          axios
            .post(api_path + "com1_agent_app/getCalllogsByContact.php", formData,{
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
        break;
        case "disposition_log":
          formData.append("mobile", req.body.mobile);
          axios
            .post(api_path + "com1_agent_app/disposition_log.php", formData,{
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
        break;
        case "agent_checkin":
          formData.append("agent_code", req.body.agent_code);
          axios
            .post(api_path + "com1_agent_app/agent_checkin.php", formData,{
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
        break;
        case "agent_checkout":
          formData.append("agent_code", req.body.agent_code);
          axios
            .post(api_path + "com1_agent_app/agent_checkout.php", formData,{
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
        break;
        case "current_checkin_status":
          formData.append("agent_code", req.body.agent_code);
          axios
            .post(api_path + "com1_agent_app/current_checked_status.php", formData,{
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
        break;
        case "history_log":
          formData.append("agent_code", req.body.agent_code);
          axios
            .post(api_path + "com1_agent_app/agent_history.php", formData,{
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
        break;
        case "dashboard":
          formData.append("agent_code", req.body.agent_code);
          axios
            .post(api_path + "com1_agent_app/dashboard.php", formData,{
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
        break;
        case "order_log":
          formData.append("mobile", req.body.mobile);
          axios
            .post(api_path + "com1_agent_app/order_log.php", formData,{
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
        break;
        case "disposition_tree":
          formData.append("disposition_code", req.body.disposition_code);
          formData.append("agent", req.body.agent);
          formData.append("agent_campaign", req.body.agent_campaign);
          formData.append("extension", req.body.extension);
          formData.append("call_status", req.body.call_status);
          axios
            .post(api_path + "com1_agent_app/disposition_list.php", formData,{
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
        break;
        case "complaint_log":
          formData.append("mobile", req.body.mobile);
          axios
            .post(api_path + "com1_agent_app/complaint_log.php", formData,{
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
        break;
        case "contact_details":
            formData.append("mobile", req.body.mobile);
            axios
              .post(api_path + "com1_agent_app/contact_details.php", formData,{
                headers: formData.getHeaders(),
              })
              .then((response) => resolve(response.data))
              .catch((err) => reject(err));
          break;   


        case "survey_data_raw":
              formData.append("campaign", req.body.campaign_code);
              axios
                .post(api_path + "surveycampaigndataraw.php", formData, {
                  headers: formData.getHeaders(),
                })
                .then((response) => resolve(response.data))
                .catch((err) => reject(err));
              break;
        case "survey_data":
              formData.append("id", req.body.id);
              axios
                .post(api_path + "surveycampaigndata.php", formData, {
                  headers: formData.getHeaders(),
                })
                .then((response) => resolve(response.data))
                .catch((err) => reject(err));
              break;
        case "survey_list":
              formData.append("status", req.body.status);
              axios
                .post(api_path + "surveycampaignlist.php", formData, {
                  headers: formData.getHeaders(),
                })
                .then((response) => resolve(response.data))
                .catch((err) => reject(err));
              break;
        case "survey_data_add":
              formData.append("campaign", req.body.campaign_code);
              formData.append("number", req.body.number);
              formData.append("data1", req.body.data1);
              formData.append("data2", req.body.data2);
              formData.append("data3", req.body.data3);
              axios
                .post(api_path + "surveydataadd.php", formData, {
                  headers: formData.getHeaders(),
                })
                .then((response) => resolve(response.data))
                .catch((err) => reject(err));
              break;
        case "cdr_day":
          formData.append("date", req.body.date);
          axios
            .post(api_path + "getcdrday.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
        case "cdr_hour":
          formData.append("hour", req.body.hour);
          axios
            .post(api_path + "getcdrhour.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
		case "send_sms":
          formData.append("send_to", req.body.send_to);
		  formData.append("msg", req.body.msg);
		  formData.append("send_port", req.body.send_port);
          axios
            .post(api_path + "sms_send_gatewaysms.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
        case "notification_register":
          formData.append("agent_code", req.body.agent_code);
          formData.append("notification_token", req.body.notification_token);
          axios
            .post(api_path + "com1_agent_app/agent_notification_register.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
        case "api_token":
          axios
            .post(api_path + "com1_agent_app/api_token.php", formData, {
              headers: formData.getHeaders(),
            })
            .then((response) => resolve(response.data))
            .catch((err) => reject(err));
          break;
        default:
          reject("Invalid action");
      }
    } else {
      reject("Action not specified.");
    }
  });

module.exports = { manageAction };
"use strict";

var express = require("express");
const cors = require('cors'); 
var app = express();
app.use(cors({
  origin: "*",
  })
);

const fs = require('fs');


const helper = require("./helper");
const responseHandler = (funtionName, req, res) => {
    try {
        // const token_settings = JSON.parse(fs.readFileSync("/var/www/html/api/configs/token.json", 'utf8'));
        // if(!(["-",""].includes(req.headers.api_token)) && !(["-",""].includes(req.headers.api_key))){
            // if(req.headers.api_token == token_settings.token_key && req.headers.api_key == token_settings.token_code){
                helper[funtionName](req).then(
                    (value) => res.send({ status: true, message: value.message, data: value.data }),
                    (error) => res.send({ status: false, message: "Error", data: error })
                );
            // } else {
            //     res.send({ status: false, message: "Unauthorised request - Token Invalid", data: "" })
            // }
        // } else {
        //     res.send({ status: false, message: "Unauthorised request - Token empty", data: "" })
        // }
    } catch (err) {
        res.send({ status: false, message: "Error", data: err })
    }
}
//setup router for API
var router = express.Router();

//configure properties route
router.route('/action').post((req, res) => { responseHandler('manageAction', req, res) });

module.exports = router;
#!/usr/bin/bash

# wget http://phone.aboss.in/repos/install-paid.sh
deploy(){

    # colors
    red=`tput setaf 1`

    green=`tput setaf 2`
    
    reset=`tput sgr0`

    PASS="Aboss@3006"

    echo -n Password: 

    read -s password

    echo ""

    if [ "$password" == "$PASS" ] ; then

    echo "${green}Started ${reset}"

    echo "${green}Installing Asterisk connecter.. ${reset}"    

    cd /var/www/html/

    wget http://phone.aboss.in/repos/asterisk-connector.tar.gz

    tar xvzf asterisk-connector.tar.gz

    rm asterisk-connector.tar.gz

    mv asterisk-connector/client/ clients

    rm -rf configs

    mkdir configs

    mv clients/$1/* configs

    rm -rf clients/

    mv configs/dev.json asterisk-connector/app/config/dev.json

    cd /var/www/html

    cd asterisk-connector

    npm i

    cd daemon

    sudo chmod +x install.sh

    sudo ./install.sh

    echo "${green}Asterisk connecter installation finished.. ${reset}"

    echo "${green}Installing COM1Pbx.. ${reset}"

    cd /var/www/html

    wget http://phone.aboss.in/repos/com1pbx.tar.gz

    tar xvzf com1pbx.tar.gz

    rm -rf com1pbx.tar.gz

    echo "${green}COM1Pbx installation finished.. ${reset}"

    echo "${green}Installing Caller.. ${reset}"

    cd /var/www/html

    wget http://phone.aboss.in/repos/caller.tar.gz

    tar xvzf caller.tar.gz

    rm caller.tar.gz

    rm -rf caller/configs
    
    mv configs/configs caller/

    mv configs/config.json caller/event_controller/config.json

    cd caller/event_controller

    npm i

    cd daemon

    sudo chmod +x install.sh

    sudo ./install.sh

    echo "${green}Caller installation finished..${reset}"

    cd /var/www/html

    rm -rf caller/customizers

    mv configs/customizers/ caller/

    mv configs/additional_files/* com1pbx/

    rm -rf /var/www/html/configs/

    echo "${green}Finished ${reset}"

    chown -R apache:apache /var/www/html/caller

    chown -R apache:apache /var/www/html/com1pbx
    
    rm /root/install-paid.sh

    else

    echo "${red}Incorrect Password ${reset}"

    fi

}

newClient(){

    read -p 'Enter client name :' client_name;

    read -p 'Enter client ip :' client_ip;

    deploy "template" $client_name $client_ip

}

echo 'Choose the client name: '
options=("Saravanastores" "Bayarea" "Rafoll")
select opt in "${options[@]}"

do
    case $opt in
        "Saravanastores")
            deploy "saravana_stores"
            break
            ;;
        "Bayarea")
            deploy "bayarea"
            break
            ;;
        "Rafoll")
            deploy "rafoll"
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done

# systemctl status asterisk-connector.service
# systemctl status caller-eventcontroller.service
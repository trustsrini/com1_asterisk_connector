"use strict";
var ami = require("./../helpers/call-listener");
function originateCall(extension, mobile, context,data_id) {
    console.log(mobile);
    var channel = context == "progressive" ? 'PJSIP/' + extension :extension;
    ami.ami.action({
        'action': 'originate',
        'channel': channel,
        'context': context,
        'Exten': mobile,
        'Priority': '1',
        'Callerid': mobile,
        'Account':data_id,
    }, function (err, res) {
        console.log(res);
        console.log(err);
    });
}
function manageEvent(event) {
    var affectedRow = event.affectedRows[0];
    var after = affectedRow.after;
    var before = affectedRow.before;
    if (after) {
        originateCall(after.extension, after.number, after.context,after.data_id);
    } else {
        console.log(before, "Deleted");
    }
    return true;
}
module.exports = { manageEvent };
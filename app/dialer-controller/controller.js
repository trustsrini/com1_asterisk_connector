"use strict";

var express = require("express");

var properties = require("./model");
const helper = require("./helper");

//setup router for API
var router = express.Router();

//Define route name
const ROUTE_NAME = "/dialer";

//configure properties route
router
    .route(ROUTE_NAME)
    //Post route to add new record
    .post((req, res) => {
        console.log(req);
    });

module.exports = router;

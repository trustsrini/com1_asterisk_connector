<?php   
	
	session_start();

    $server = "localhost";
    
    $user_name = "root";
    
    $password = "#*1234Abcd*#";
    
    $database = "com1system";

    $connection = mysqli_connect($server, $user_name, $password, $database) or die(mysqli_error($connection));

    $extension = $_POST['extension'] != '' ? $_POST['extension'] : $_SESSION['extension'];

    $query = "CALL com1user.`usp_user_directory_bindgrid_transferpopup`('$extension')";

    $campaign_name_query_exe = mysqli_query($connection, $query);

    $call_id = $_SESSION['last_call_details']['call_id'];
	
	$mobile = $_SESSION['last_call_details']['data_mobile'];

    $phone_type = $_POST['phone_type'] != '' ? $_POST['phone_type'] : $_SESSION['phonetype'];

    $placeholder = $phone_type != 'WEBRTC' ? "Dial extension" : "Dial number or extension";
    
    $blindtransfer = $phone_type != 'WEBRTC' ? "extension_blindtransfer()" : "webrtc_call('blindtransfer')";

    $attended_transfer = $phone_type != 'WEBRTC' ? "extension_atxtransfer()" : "webrtc_call('atxtransfer')";

    $confirme_attended_transfer = $phone_type != 'WEBRTC' ? "call_hangup('$call_id','$mobile')" : "webrtc_call('atxtransfer_confirmation','yes')";

    $cancel_attended_transfer = $phone_type != 'WEBRTC' ? "call_cancel_transfer()" : "webrtc_call('atxtransfer_confirmation','no')";

    $login_details = json_decode($_SESSION['login_details']);

    $dial_digit = $login_details -> DialDigits;

?>
<style>
    #call_dial_input {
        height: auto;
    }
</style>
<div class="text-nowrap table-responsive" id="caller_extansion_form">

<?php if(isset($_POST['extension']) && $_POST['extension'] != '') { ?>
    <input type="text" class="form-control" placeholder="Extension" id="extension" value="<?php echo $_POST['extension']; ?>" hidden>
<?php } ?> 
    <div class="d-flex justify-content-between">
        <div class="d-flex">
            <input type="number" class="form-control call_dial calling_btn" placeholder="<?php echo $placeholder; ?>" id="call_dial_input">           
            <button class= "btn btn-sm btn-success py-0 ml-2 call_dial calling_btn" id="" onclick="<?php echo $attended_transfer; ?>" aria-hidden="true" alt="attended_transfer" width="15" data-toggle="tooltip" data-placement="top" title="Attended Transfer"><i class="fa fa-phone text-white"></i></button>
            <button class= "btn btn-sm btn-info py-0 mx-2 call_transfer" style="display:none" onclick="<?php echo $confirme_attended_transfer; ?>"><img src='/asterisk-connector/app/call-buttons/icons/call-transfer.png' alt='attended_transfer' width='15' data-toggle="tooltip" data-placement="top" title="Attended transfer"></button>
            <button class= "btn btn-sm btn-danger py-0 call_transfer" style="display:none" onclick="<?php echo $cancel_attended_transfer; ?>"><img src='/asterisk-connector/app/call-buttons/icons/call-transfer.png' alt='cancel_transfer' width='15' data-toggle="tooltip" data-placement="top" title="Cancel Transfer"></button>                    
            <button class= "btn btn-sm btn-info mx-2 call calling_btn"><img src="/asterisk-connector/app/call-buttons/icons/call-transfer.png" onclick="<?php echo $blindtransfer; ?>" alt="transfer" width="15" data-toggle="tooltip" data-placement="top" title="Blind Transfer"></button>
            <?php if($phone_type !='WEBRTC') { ?>
                <button class="btn btn-sm btn-info calling_btn" id="call_hold_transfer"  onclick="call_hold('transfer')" alt="hold" data-toggle="tooltip" data-placement="top" title="Hold"><img src="/asterisk-connector/app/call-buttons/icons/call-hold.png" width="15"></button>
                <input type="number" class="form-control call_dial" style="display:none" placeholder="Dial number" id="transfer_call_dial">
                <div class="btn bg-success mx-2 call_dial" id="ext_transfer_call" style="display:none" onclick="extTransferCall()" data-toggle="tooltip" data-placement="top" title="Dial">
                    <i class="fa fa-phone text-white" aria-hidden="true"></i>
                </div> 
                <button class="btn btn-sm btn-info mx-2 call_transfer_icon" style="display:none"><img src="/asterisk-connector/app/call-buttons/icons/call-transfer.png" onclick="external_transfer()" alt="hold" width="15" data-toggle="tooltip" data-placement="top" title="External Transfer"></button>
            <?php } else { ?>
                <button class= "btn btn-sm btn-success py-0 call_dial calling_btn" id="conference_call" onclick="webrtc_call('conference')"><img src='/asterisk-connector/app/call-buttons/icons/call-conference.png' alt='conference' width='15' data-toggle="tooltip" data-placement="top" title="Conference"></button>
                <button class= "btn btn-sm btn-info py-0 mx-2 call_conference" style="display:none" onclick="webrtc_call('conference_confirmation','yes')"><img src='/asterisk-connector/app/call-buttons/icons/call-conference.png' alt='conference' width='15' data-toggle="tooltip" data-placement="top" title="Conference"></button>
                <button class= "btn btn-sm btn-danger py-0 mr-2 call_conference" style="display:none" onclick="webrtc_call('conference_confirmation','no')"><img src='/asterisk-connector/app/call-buttons/icons/call-conference.png' alt='cancel_conference' width='15' data-toggle="tooltip" data-placement="top" title="Cancel conference"></button>
            <?php } ?>
        </div>
        <div class="has-search">
            <span class="fa fa-search form-control-feedback"></span> 
            <input type="text" class="form-control" placeholder="Search" id="table-search">
        </div>
    </div> 
 <table class="table table-hover table-striped mt-3" style="width:100%;">
    <tr class="bg-dark text-white">
        <th>Name</th>
        <th>Extension</th>
        <th>Mobile</th>
        <th>Alternate</th>
    </tr>
    <tbody id="myTable">
        <?php 
        $i='1';
        while ($row = mysqli_fetch_assoc($campaign_name_query_exe)) { 

            $extension_blindtransfer =  $phone_type != 'WEBRTC' ? "extension_blindtransfer('$row[extension]')" : "webrtc_call('blindtransfer','ext_call','$row[extension]')";
            
            $extension_atxtransfer =  $phone_type != 'WEBRTC' ? "extension_atxtransfer('$row[extension]','ext_call')" : "webrtc_call('atxtransfer','ext_call','$row[extension]')";
            
        ?>
        <tr>
            <td style="width:200px;"><?php echo $row['name'] ?></td>
            <td >
                <div class="d-flex justify-content-between align-items-center">
                    <div class="<?php echo $row['extcolour'];?> p-1 rounded">
                        <?php echo $row['extension'] != 0 ? $row['extension'] : ''; ?>
                    </div>                        
                    <div class="ml-2">
                        <?php if($row['extension'] != 0) {  ?>
                            <button class="btn btn-sm btn-success call_dial ext_call calling_btn" id="" style="font-size:10px" onclick="<?php echo $extension_atxtransfer; ?>" aria-hidden="true" alt="attended_transfer" data-toggle="tooltip" data-placement="top" title="Attended Transfer"><i class="fa fa-phone text-white"></i></button>
                            <button class= "btn btn-sm btn-info py-0 mx-2 ext_call_transfer_<?php echo $row['extension'] ?>" style="display:none" onclick="<?php echo $confirme_attended_transfer; ?>"><img src='/asterisk-connector/app/call-buttons/icons/call-transfer.png' alt='attended_transfer' width='15' data-toggle="tooltip" data-placement="top" title="Attended transfer"></button>
                            <button class= "btn btn-sm btn-danger py-0 ext_call_transfer_<?php echo $row['extension'] ?>" style="display:none" onclick="<?php echo $cancel_attended_transfer; ?>"><img src='/asterisk-connector/app/call-buttons/icons/call-transfer.png' alt='cancel_transfer' width='15' data-toggle="tooltip" data-placement="top" title="Cancel Transfer"></button>
                            <button class="btn btn-sm btn-info ext_call calling_btn" style="font-size:10px"><img src="/asterisk-connector/app/call-buttons/icons/call-transfer.png" onclick="<?php echo $extension_blindtransfer; ?>" alt="transfer" width="13" data-toggle="tooltip" data-placement="top" title="Blind Transfer"></button>
                            <?php if($phone_type =='WEBRTC') { ?>
                                <button class= "btn btn-sm btn-success py-0 calling_btn" onclick="webrtc_call('conference','ext_call','<?php echo $row['extension'] ?>')"><img src='/asterisk-connector/app/call-buttons/icons/call-conference.png' alt='conference' width='18' data-toggle="tooltip" data-placement="top" title="Conference"></button>
                                <button class= "btn btn-sm btn-info py-0 mx-2 ext_call_conference_<?php echo $row['extension'] ?>" style="display:none" onclick="webrtc_call('conference_confirmation','yes')"><img src='/asterisk-connector/app/call-buttons/icons/call-conference.png' alt='conference' width='18' data-toggle="tooltip" data-placement="top" title="Conference"></button>
                                <button class= "btn btn-sm btn-danger py-0 mr-2 ext_call_conference_<?php echo $row['extension'] ?>" style="display:none" onclick="webrtc_call('conference_confirmation','no')"><img src='/asterisk-connector/app/call-buttons/icons/call-conference.png' alt='cancel_conference' width='18' data-toggle="tooltip" data-placement="top" title="Cancel conference"></button>
                        <?php } } ?>
                    <div>
                <div>
            </td>
            <td>
                <div class="d-flex justify-content-between align-items-center">
                    <?php 
                        
                        echo $row['number1'] != 0 ? $row['number1'] : '';
                        
                        $numlength_1 = strlen((string)$row['number1']);
                        
                        if($row['number1'] != '' && $numlength_1  >= $dial_digit) { 

                    ?>
                        <div class="ml-2">               
                            <?php if($phone_type !='WEBRTC') { ?> 
                            <button class="btn btn-sm btn-info mx-2 calling_btn call_hold_1<?php echo $i; ?>" onclick="call_hold('transfer','<?php echo '1'.$i; ?>')"><img src="/asterisk-connector/app/call-buttons/icons/call-hold.png" alt="hold" width="15" data-toggle="tooltip" data-placement="top" title="Hold"></button>                   
                            <button class="btn btn-sm btn-success mx-0 call_btn_1<?php echo $i; ?>" style="display:none;" onclick ="extTransferCall(`<?php echo substr($row['number1'], '-'.$dial_digit) ?>`,'<?php echo '1'.$i; ?>')"><i class="fa fa-phone text-white" aria-hidden="true"  data-toggle="tooltip" data-placement="top" title="Transfer"></i></button>
                            <button class="btn btn-sm btn-info mx-2 call_transfer_icon_1<?php echo $i; ?>" style="display:none" onclick="external_transfer()"><img src="/asterisk-connector/app/call-buttons/icons/call-transfer.png" alt="hold" width="15" data-toggle="tooltip" data-placement="top" title="External Transfer"></button>
                            <?php } else { ?>
                                <button class="btn btn-sm btn-success call_dial mobile_call calling_btn" id="" style="font-size:10px" onclick="webrtc_call('atxtransfer','mobile_call',`<?php echo substr($row['number1'], '-'.$dial_digit) ?>`)" aria-hidden="true" alt="attended_transfer" data-toggle="tooltip" data-placement="top" title="Attended Transfer"><i class="fa fa-phone text-white"></i></button>
                                <button class="btn btn-sm btn-info mobile_call calling_btn" style="font-size:10px"><img src="/asterisk-connector/app/call-buttons/icons/call-transfer.png" onclick="webrtc_call('blindtransfer','',`<?php echo substr($row['number1'], '-'.$dial_digit) ?>`)" alt="transfer" width="13" data-toggle="tooltip" data-placement="top" title="Blind Transfer"></button>
                                <button class="btn btn-sm btn-info mobile_call_transfer_<?php echo substr($row['number1'], '-'.$dial_digit) ?>" style="display:none;font-size:10px" onclick="webrtc_call('atxtransfer_confirmation','yes')"><img src='/asterisk-connector/app/call-buttons/icons/call-transfer.png' alt='attended_transfer' width='13' data-toggle="tooltip" data-placement="top" title="Attended transfer"></button>
                                <button class="btn btn-sm btn-danger mobile_call_transfer_<?php echo substr($row['number1'], '-'.$dial_digit) ?>" style="display:none;font-size:10px" onclick="webrtc_call('atxtransfer_confirmation','no')"><img src='/asterisk-connector/app/call-buttons/icons/call-transfer.png' alt='cancel_transfer' width='13' data-toggle="tooltip" data-placement="top" title="Cancel Transfer"></button>                            
                                <button class= "btn btn-sm btn-success py-0 calling_btn" onclick="webrtc_call('conference','mobile_call','<?php echo substr($row['number1'], '-'.$dial_digit) ?>')"><img src='/asterisk-connector/app/call-buttons/icons/call-conference.png' alt='conference' width='18' data-toggle="tooltip" data-placement="top" title="Conference"></button>
                                <button class= "btn btn-sm btn-info py-0 mx-2 mobile_call_conference_<?php echo substr($row['number1'], '-'.$dial_digit) ?>" style="display:none" onclick="webrtc_call('conference_confirmation','yes')"><img src='/asterisk-connector/app/call-buttons/icons/call-conference.png' alt='conference' width='18' data-toggle="tooltip" data-placement="top" title="Conference"></button>
                                <button class= "btn btn-sm btn-danger py-0 mr-2 mobile_call_conference_<?php echo substr($row['number1'], '-'.$dial_digit) ?>" style="display:none" onclick="webrtc_call('conference_confirmation','no')"><img src='/asterisk-connector/app/call-buttons/icons/call-conference.png' alt='cancel_conference' width='18' data-toggle="tooltip" data-placement="top" title="Cancel conference"></button>
                            <?php } ?>
                        </div>            
                    <?php } ?>            
                </div>
            </td>
            <td>
                <div class="d-flex justify-content-between align-items-center">
                    <?php 
                    
                        echo $row['number2'] != 0 ? $row['number2'] : '';
                        
                        $numlength_2 = strlen((string)$row['number2']);
                        
                        if($row['number2'] != '' && $numlength_2  >= $dial_digit) {                                        
                    ?>
                        <div class="ml-2">               
                            <?php if($phone_type !='WEBRTC') { ?> 
                            <button class="btn btn-sm btn-info mx-2 calling_btn call_hold_2<?php echo $i; ?>" onclick="call_hold('transfer','<?php echo '2'.$i; ?>')"><img src="/asterisk-connector/app/call-buttons/icons/call-hold.png" alt="hold" width="15" data-toggle="tooltip" data-placement="top" title="Hold"></button>                   
                            <button class="btn btn-sm btn-success mx-0 call_btn_2<?php echo $i; ?>" style="display:none;" onclick ="extTransferCall(`<?php echo substr($row['number2'], '-'.$dial_digit) ?>`,'<?php echo '2'.$i; ?>')"><i class="fa fa-phone text-white" aria-hidden="true"  data-toggle="tooltip" data-placement="top" title="Transfer"></i></button>
                            <button class="btn btn-sm btn-info mx-2 call_transfer_icon_2<?php echo $i; ?>" style="display:none" onclick="external_transfer()"><img src="/asterisk-connector/app/call-buttons/icons/call-transfer.png" alt="hold" width="15" data-toggle="tooltip" data-placement="top" title="External Transfer"></button>                    
                            <?php } else { ?>
                                <button class="btn btn-sm btn-success call_dial alternate_call calling_btn" style="font-size:10px" id="" onclick="webrtc_call('atxtransfer','alternate_call',`<?php echo $row['number2'] ?>`)" aria-hidden="true" alt="attended_transfer" data-toggle="tooltip" data-placement="top" title="Attended Transfer"><i class="fa fa-phone text-white"></i></button>
                                <button class="btn btn-sm btn-info alternate_call calling_btn" style="font-size:10px"><img src="/asterisk-connector/app/call-buttons/icons/call-transfer.png" onclick="webrtc_call('blindtransfer','',`<?php echo $row['number2'] ?>`)" alt="transfer" width="13" data-toggle="tooltip" data-placement="top" title="Blind Transfer"></button>
                                <button class="btn btn-sm btn-info alternate_call_transfer_<?php echo $row['number2'] ?>" style="display:none;font-size:10px" onclick="webrtc_call('atxtransfer_confirmation','yes')"><img src='/asterisk-connector/app/call-buttons/icons/call-transfer.png' alt='attended_transfer' width='13' data-toggle="tooltip" data-placement="top" title="Attended transfer"></button>
                                <button class="btn btn-sm btn-danger alternate_call_transfer_<?php echo $row['number2'] ?>" style="display:none;font-size:10px" onclick="webrtc_call('atxtransfer_confirmation','no')"><img src='/asterisk-connector/app/call-buttons/icons/call-transfer.png' alt='cancel_transfer' width='13' data-toggle="tooltip" data-placement="top" title="Cancel Transfer"></button>
                                <button class= "btn btn-sm btn-success py-0 calling_btn" onclick="webrtc_call('conference','mobile_call','<?php echo substr($row['number2'], '-'.$dial_digit) ?>')"><img src='/asterisk-connector/app/call-buttons/icons/call-conference.png' alt='conference' width='18' data-toggle="tooltip" data-placement="top" title="Conference"></button>
                                <button class= "btn btn-sm btn-info py-0 mx-2 mobile_call_conference_<?php echo substr($row['number2'], '-'.$dial_digit) ?>" style="display:none" onclick="webrtc_call('conference_confirmation','yes')"><img src='/asterisk-connector/app/call-buttons/icons/call-conference.png' alt='conference' width='18' data-toggle="tooltip" data-placement="top" title="Conference"></button>
                                <button class= "btn btn-sm btn-danger py-0 mr-2 mobile_call_conference_<?php echo substr($row['number2'], '-'.$dial_digit) ?>" style="display:none" onclick="webrtc_call('conference_confirmation','no')"><img src='/asterisk-connector/app/call-buttons/icons/call-conference.png' alt='cancel_conference' width='18' data-toggle="tooltip" data-placement="top" title="Cancel conference"></button>                    
                            <?php } ?>
                        </div>                 
                    <?php } ?>
                </div>
            </td>
        </tr>
        <?php  $i++; }?>
    </tbody>
</table>
</div>
<script>
 $("#table-search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });

</script>
  

